#!/usr/bin/octave -qf

% # audio tamper - audio tampering finder based on MFCC
% #     Copyright (C) 2014  Matteo Nunziati

% #     This program is free software: you can redistribute it and/or modify
% #     it under the terms of the GNU General Public License as published by
% #     the Free Software Foundation, either version 3 of the License, or
% #     any later version.

% #     This program is distributed in the hope that it will be useful,
% #     but WITHOUT ANY WARRANTY; without even the implied warranty of
% #     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% #     GNU General Public License for more details.

% #     You should have received a copy of the GNU General Public License
% #     along with this program.  If not, see <http://www.gnu.org/licenses/>.

%parse input
arg_list = argv();
fname=arg_list{1};
lower_q = str2num(arg_list{2});
min_activation = str2num(arg_list{3});

%load data
M=load(fname, '-ascii');
MF=reshape(M,14, length(M)/14);

%get MFCC and C0
MFCC=MF(3:end,:);
C0=MF(2,:);

%compute delta and delta-delta
V=abs(diff(MFCC,1,2));
A=abs(diff(MFCC,2,2));

%normalize values
C0n = (C0 -min(C0))./(max(C0)-min(C0));
Vn = (V-min(V')')./(max(V')'-min(V')');
An = (A-min(A')')./(max(A')'-min(A')');

%find for dubious points: low energy level + high frequency changes!
cum_thr_speed = sum(Vn)/12;
cum_thr_acc = sum(An)/12;

doubt_comb = (cum_thr_speed(1:end-1) + cum_thr_acc)/2;

higher_q = 1-lower_q;
cq = quantile (C0n', lower_q);

CI = C0n <= cq;
activation_level = doubt_comb;% .* CI(1:end-2); %this set to 0 any activation at higher volumes

doubt_ = find(activation_level > min_activation);

%evaluate doubt intervals considering window sampling (hardcoded in praat script!)
lb_sec = 0.015 * (doubt_-1);
ub_sec = 0.015 * (doubt_-1) + 0.03;

%print to stdout
disp('index,start [sec],end [sec],activation level [0-1]')
for i=1:length(doubt_)
	act = activation_level(doubt_(i));
	disp([int2str(i) ',' num2str(lb_sec(i)) ',' num2str(ub_sec(i)) ',' num2str(act)])
end

%perform some prints
%TO DO: very wide intervals between tampered regions let the plot to be useless