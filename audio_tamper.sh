# audio tamper - audio tampering finder based on MFCC
#     Copyright (C) 2014  Matteo Nunziati

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.


echo -e "$0  Copyright (C) 2014  Matteo Nunziati\n
This program comes with ABSOLUTELY NO WARRANTY.\n
This is free software, and you are welcome to redistribute it\n
under GPL3+ license.\n"

#
#default options
#

LOWER_Q='0.25'
ACTIV='0.50'

#
# aux functions
#

function usage {
	echo "usage:"
	echo ""
	echo "$0 -i input_file [-o output_report_file] [-l val] [-a active_coef]"
	echo "where input_file must be a valid wav PCM 16-bit mono audio file"
	echo "optional output_report_file is a csv file with outlayers"
	echo "*UNUSED* optional val must be a number between 0 and 1, used to define the lower quantile which activates tampering warnings"
	echo "optional active_coef must be a number between 0 and 1, used to define the min amount of activation to be detected to raise a tampering warning"
}

#
# cmd line parsing
#

#never say never
if [ $# -lt 2 ]; then
	usage
	exit 1
fi

#parse command line
TMP=$(getopt -n $0 -o i:o:c:hl:a: --long help  -- "$@")
eval set -- "$TMP"

IN_FILE=""
OUT_FILE=""
CONF_FILE=""
CWD=$(pwd)

while true; do
	case $1 in
		-i)
			IN_FILE="$2"; shift 2; continue
		;;
		-o)
			OUT_FILE="$2"; shift 2; continue
		;;
		-c)
			CONF_FILE="$2"; shift 2; continue
		;;
		-l)
			LOWER_Q="$2"; shift 2; continue
		;;
		-a)
			ACTIV="$2"; shift 2; continue
		;;

		-h|--help)
			usage
			exit 0
		;;
		--)
			# no more arguments to parse
			break
		;;
		*)
			printf "Unknown option %s\n" "$1"
			exit 1
		;;
	esac
done

if [ "$IN_FILE" = "" ]; then
	usage
	exit 1
fi


FNAME=$(basename $IN_FILE .wav)
DNAME=$(dirname $IN_FILE)

if [ "$OUT_FILE" = "" ]; then
	OUT_FILE=$DNAME"/"$FNAME".csv"
fi

#
#let the fun begin!
#

TMPNAME=$DNAME"/"$FNAME".MFCC"
TMP2FILE=$DNAME"/"$FNAME".tmp"

#extract mfcc with praat
praat &
PID=$!
sleep 2 #loading, please wait :-)
sendpraat 0 praat "execute $CWD/extract.praat $DNAME $FNAME wav"
wait $PID

#clean output
NUM_OF_LINES=$(wc -l $TMPNAME | cut -f1 -d " ")
HEAD=11
TAIL_LINES=$(echo "$NUM_OF_LINES - $HEAD" | bc -l)
tail -$TAIL_LINES $TMPNAME > $TMP2FILE

#process with octave
./processor.m $TMP2FILE $LOWER_Q $ACTIV > $OUT_FILE

#cleanup!
rm $TMP2FILE
rm $TMPNAME