requires:

- a running linux distro (devel started on debian jessie/sid)
- praat + sendpraat (for mfcc)
- octave (for computation)
- a valid bash compatible shell