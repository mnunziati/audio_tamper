# audio tamper - audio tampering finder based on MFCC
#     Copyright (C) 2014  Matteo Nunziati

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

form input
	text working_dir
	text file_name
	text file_extension
endform

path_n_base$ = working_dir$ + "/" + file_name$
file$ = path_n_base$ + "." + file_extension$
output$ = path_n_base$ + ".MFCC"

do ("Read from file...",file$)
do ("To MFCC...", 12, 0.03, 0.015, 100, 100, 0)
do ("Save as short text file...", output$)

Quit



